# SpringSeason

## Database config
Open the sql shell and follow these commands
`    CREATE USER test;
    ALTER ROLE test WITH CREATEDB;
    CREATE DATABASE test OWNER test;
    ALTER USER test WITH ENCRYPTED PASSWORD 'test' ;`

## Launch of the website
The launch of this site is done on **IntelliJ**. You just have to run the file __Application.java__

## Connect as an admin
    login: admin
    password: admin

## Demo of the website
https://ensgeu-my.sharepoint.com/:v:/g/personal/jessy_ribaira_ensg_eu/EUPsWXcNZ_RCpm3K5KETsg8BWLq6qsb8cHNfsEDtxgx5ag?e=N4ZIFN
