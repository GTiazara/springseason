let checkbox = document.getElementById("checkbox");
let phoneNb = document.getElementById("Organizer_phoneNumber");
let title = document.getElementById("Organizer_Title");

checkbox.addEventListener("change", () => {
    if (checkbox.checked) {
        phoneNb.innerHTML = `<input type="text" th:field="*{phoneNumber}" /><br />
        <span>Phone number:</span>`;
        title.innerHTML = `<input type="text" th:field="*{title}" /><br />
        <span>Title:</span>`;

    }
})