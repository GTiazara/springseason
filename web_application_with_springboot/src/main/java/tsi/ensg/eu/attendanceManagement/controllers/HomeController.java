package tsi.ensg.eu.attendanceManagement.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
    @GetMapping("/home")
    public String index() {
        return "home";
    }

    @GetMapping("/")
    public String home() {
        return "home";
    }

}
