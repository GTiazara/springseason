package tsi.ensg.eu.attendanceManagement.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "event")
public class Event {
    @Id
    @GeneratedValue(generator = "incerement")
    @GenericGenerator(name = "increment2", strategy = "increment")
    @Column(name = "id")
    private long id;
    @Column(name = "titre", nullable = false)
    private String titre;
    @Column(name = "theme", nullable = false)
    private String theme;
    @Column(name = "date_debut", nullable = false)
    private String date_debut;
    @Column(name = "duree_jours", nullable = false)
    private String duree_jours;
    @Column(name = "max_participant_number", nullable = false)
    private int max_participant_number;
    @Column(name = "description", nullable = false)
    private String description;
    @Column(name = "type_event", nullable = false)
    private String type_event;

    @ManyToMany(mappedBy = "events", fetch = FetchType.LAZY)
    @JsonIgnore
    @Column(name = "participants", nullable = false)
    private Set<Participant> participants = new HashSet<>();

    public Event() {
    }

    public Event(String titre, String theme, String date_debut, String duree_jours, int max_participant_number,
            String description, String type_event) {
        this.titre = titre;
        this.theme = theme;
        this.date_debut = date_debut;
        this.duree_jours = duree_jours;
        this.max_participant_number = max_participant_number;
        this.description = description;
        this.type_event = type_event;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public int getMax_participant_number() {
        return max_participant_number;
    }

    public void setMax_participant_number(int max_participant_number) {
        this.max_participant_number = max_participant_number;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType_event() {
        return type_event;
    }

    public void setType_event(String type_event) {
        this.type_event = type_event;
    }

    public String getDate_debut() {
        return date_debut;
    }

    public void setDate_debut(String date_debut) {
        this.date_debut = date_debut;
    }

    public String getDuree_jours() {
        return duree_jours;
    }

    public void setDuree_jours(String duree_jours) {
        this.duree_jours = duree_jours;
    }

    public Set<Participant> getParticipants() {
        return participants;
    }

    public void setParticipants(Set<Participant> participants) {
        this.participants = participants;
    }

    public int getNbParticipants() {
        return participants.size();
    }

    @Override
    public String toString() {
        return this.getId() + ":" + titre;
    }

    public void addParticipant(Participant participant) {
        this.participants.add(participant);
        participant.getEvents().add(this);
    }

    public void removeParticipant(long participantId) {
        Participant participant = this.participants
                .stream()
                .filter(participant1 -> participant1.getId() == participantId)
                .findFirst()
                .orElse(null);
        if (participant != null) {
            this.participants.remove(participant);
            participant.getEvents().remove(this);
        }
    }

}