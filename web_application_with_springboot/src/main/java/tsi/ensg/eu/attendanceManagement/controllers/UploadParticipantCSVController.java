package tsi.ensg.eu.attendanceManagement.controllers;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import tsi.ensg.eu.attendanceManagement.models.Event;
import tsi.ensg.eu.attendanceManagement.models.Participant;
import tsi.ensg.eu.attendanceManagement.services.EventService;
import tsi.ensg.eu.attendanceManagement.services.ParticipantService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

@Controller
public class UploadParticipantCSVController {

    @Autowired
    ParticipantService participantService;

    @PostMapping("/postCsvParticipantData")
    public String uploadCSVFile(@RequestParam("file") MultipartFile file, Model model) {

        // validate file
        if (file.isEmpty()) {
            model.addAttribute("message", "Please select a CSV file to upload for particpant.");
            model.addAttribute("status", false);
        } else {

            // parse CSV file to create a list of `User` objects
            try (Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {

                // create csv bean reader
                CsvToBean csvToBean = new CsvToBeanBuilder(reader)
                        .withType(Participant.class)
                        .withIgnoreLeadingWhiteSpace(true)
                        .build();


                // convert `CsvToBean` object to list of users
                List<Participant> participants = csvToBean.parse();

                participants.forEach(participant -> {
                    System.out.println("rqsrzaerazer");
                    System.out.println(participant.toString());
                    participantService.save(participant);});

                // TODO: save users in DB?

                // save users list on model
                model.addAttribute("participants", participants);
                model.addAttribute("status", true);

            } catch (Exception ex) {
                model.addAttribute("message", "An error occurred while processing the CSV file.");
                model.addAttribute("status", false);
            }
        }

        return "redirect:/participants";
    }
}
