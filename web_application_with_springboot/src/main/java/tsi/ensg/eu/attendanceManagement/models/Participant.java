package tsi.ensg.eu.attendanceManagement.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "participant")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Participant {
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    @Column(name = "id")
    private long id;
    @Column(name = "firstName", nullable = false)
    private String firstName;
    @Column(name = "lastName", nullable = false)
    private String lastName;
    @Column(name = "emailAddress", nullable = false)
    private String emailAddress;
    @Column(name = "dateBirth", nullable = false)
    private String dateBirth;
    @Column(name = "organisation", nullable = false)
    private String organisation;
    @Column(name = "observation", nullable = false)
    private String observation;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinTable(name = "participants_events", joinColumns = {
            @JoinColumn(name = "participants_id", nullable = false, updatable = false) }, inverseJoinColumns = {
                    @JoinColumn(name = "eventss_id", nullable = false, updatable = false) })
    @Column(name = "events", nullable = true)
    private Set<Event> events = new HashSet<>();

    public Participant() {
    }

    public Participant(String firstName, String lastName, String emailAddress, String dateBirth, String organisation,
            String observation, Boolean is_Organizer) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
        this.dateBirth = dateBirth;
        this.organisation = organisation;
        this.observation = observation;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getDateBirth() {
        return dateBirth;
    }

    public void setDateBirth(String dateBirth) {
        this.dateBirth = dateBirth;
    }

    public String getOrganisation() {
        return organisation;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Set<Event> getEvents() {
        return events;
    }

    public void setEvents(Set<Event> events) {
        this.events = events;
    }

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }

    public void addEvent(Event event) {
        this.events.add(event);
        event.getParticipants().add(this);
    }

    public void removeEvent(long eventId) {
        Event event = this.events
                .stream()
                .filter(event1 -> event1.getId() == eventId)
                .findFirst()
                .orElse(null);
        if (event != null) {
            this.events.remove(event);
            event.getParticipants().remove(this);
        }
    }
}