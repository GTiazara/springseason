package tsi.ensg.eu.attendanceManagement.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tsi.ensg.eu.attendanceManagement.models.Participant;
import tsi.ensg.eu.attendanceManagement.repositories.ParticipantRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ParticipantService {

    @Autowired
    private ParticipantRepository repository;

    public List<Participant> findAll() {
        List<Participant> participants = (List<Participant>) repository.findAll();
        return participants;
    }

    public void save(Participant participant) { // pas testé avec persist !
        repository.save(participant);
    }

    public Optional<Participant> findById(Long id) {
        return repository.findById(id);
    }

    public void delete(Participant participant) {
        repository.delete(participant);
    }

}
