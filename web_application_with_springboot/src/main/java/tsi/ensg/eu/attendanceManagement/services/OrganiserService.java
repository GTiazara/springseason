package tsi.ensg.eu.attendanceManagement.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tsi.ensg.eu.attendanceManagement.models.Organizer;
import tsi.ensg.eu.attendanceManagement.repositories.OrganiserRepository;

import java.util.List;
import java.util.Optional;

@Service
public class OrganiserService {
    @Autowired
    private OrganiserRepository repository;

    public List<Organizer> findAll() {
        List<Organizer> organizers = (List<Organizer>) repository.findAll();
        return organizers;
    }

    public void save(Organizer organizer) { // pas testé avec persist !
        repository.save(organizer);
    }

    public Optional<Organizer> findById(Long id) {
        return repository.findById(id);
    }

    public void delete(Organizer organizer) {
        repository.delete(organizer);
    }

    /*
     * public boolean update(Participant participant, List<Event> events) {
     * participant.setEvents((Set<Event>) events);
     * return true;
     * }
     */

}
