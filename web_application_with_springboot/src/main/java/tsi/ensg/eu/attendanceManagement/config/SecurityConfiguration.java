package tsi.ensg.eu.attendanceManagement.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;

//ressource
//https://howtodoinjava.com/spring5/security/login-form-example/
//https://www.javadevjournal.com/spring-security/spring-security-login/
//https://www.tutorialspoint.com/spring_security/spring_security_form_login_with_database.htm
//https://openclassrooms.com/fr/courses/7137776-securisez-votre-application-web-avec-spring-security/7275511-configurez-une-page-de-connexion-customisee-avec-spring-security
//https://mkyong.com/spring-security/customize-http-403-access-denied-page-in-spring-security/
//https://mkyong.com/spring-security/spring-security-form-login-example/
//https://mkyong.com/spring-security/spring-security-form-login-using-database/
@Configuration
public class SecurityConfiguration {

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/","/home", "/joinEvent", "/joinEventAsOrganiser", "/css/**", "/success_join_event_as_organiser", "/deleteEvent/**")
                .permitAll()
                .and()
                .authorizeRequests()
                .antMatchers("/events", "/addEvent", "/joinEventAdmin", "/joinEventAsOrganiserAdmin")
                .authenticated()
                .and()
                .formLogin();
                //.httpBasic()
        return http.build();
    }
}