package tsi.ensg.eu.attendanceManagement.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import tsi.ensg.eu.attendanceManagement.models.Event;
import tsi.ensg.eu.attendanceManagement.models.Organizer;
import tsi.ensg.eu.attendanceManagement.models.Participant;
import tsi.ensg.eu.attendanceManagement.services.EventService;
import tsi.ensg.eu.attendanceManagement.services.ParticipantService;

import java.util.ArrayList;
import java.util.List;

@Controller
public class ParticipantController {
    @Autowired
    ParticipantService participantService;
    @Autowired
    EventService eventService;

    @GetMapping("/joinEvent")
    public String addParticipant(Model model) {
        /* Creation of an participant through a form */
        Participant participant = new Participant();
        model.addAttribute("participant", participant);
        List<Event> events = eventService.findAll();
        model.addAttribute("listEvents", events);
        return "joinEvent";
    }

    @GetMapping("/joinEventAdmin")
    public String addParticipantAdmin(Model model) {
        /* Creation of an participant through a form */
        Participant participant = new Participant();
        model.addAttribute("participant", participant);
        List<Event> events = eventService.findAll();
        model.addAttribute("listEvents", events);
        return "joinEvent";
    }

    @PostMapping("/postJoinEventParticipant")
    public String submitFormParticipant(@ModelAttribute("participant") Participant participant) {
        /* Submits info on the created participant */
        participantService.save(participant);
        System.out.println(participant);
        return "success_join_event";
    }

    @RequestMapping(value = "/deleteParticipant/{id}")
    private String deleteParticipant(@PathVariable(name = "id") int id) {
        /* Deletes participant */
        System.out.println("Participant_Id : " + id);
        participantService.delete(participantService.findById((long) (id)).get());
        return "redirect:/participants";
    }

    @GetMapping("/participants")
    public String getParticipant(Model model) {
        /* Display lists of participants */
        List<Participant> list = new ArrayList<>();

        for (Participant p : participantService.findAll()) {
            if (!(p instanceof Organizer)) {
                list.add(p);
            }
        }

        model.addAttribute("participants", list);
        return "participants";
    }

    @GetMapping("/participant/{id}")
    public String updateFormParticipant(@PathVariable("id") int id, Model model) {
        /* Display information on a particular participant */
        Participant participant = participantService.findById((long) (id)).get();
        model.addAttribute("participant", participant);

        List<Event> events = eventService.findAll();
        model.addAttribute("listEvents", events);

        return "joinEvent";
    }

}
