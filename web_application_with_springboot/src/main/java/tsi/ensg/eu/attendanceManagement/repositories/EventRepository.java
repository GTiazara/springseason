package tsi.ensg.eu.attendanceManagement.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tsi.ensg.eu.attendanceManagement.models.Event;

import java.util.List;
@Repository
public interface EventRepository extends CrudRepository<Event, Long> {
    List<Event> findEventById(Long id);
}
