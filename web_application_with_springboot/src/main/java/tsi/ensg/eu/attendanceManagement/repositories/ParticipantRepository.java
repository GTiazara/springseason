package tsi.ensg.eu.attendanceManagement.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tsi.ensg.eu.attendanceManagement.models.Participant;

@Repository
public interface ParticipantRepository extends CrudRepository<Participant, Long> {
}
