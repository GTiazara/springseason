package tsi.ensg.eu.attendanceManagement.repositories;

import org.springframework.data.repository.CrudRepository;
import tsi.ensg.eu.attendanceManagement.models.Organizer;

public interface OrganiserRepository extends CrudRepository<Organizer, Long> {
}