package tsi.ensg.eu.attendanceManagement.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import tsi.ensg.eu.attendanceManagement.models.Event;
import tsi.ensg.eu.attendanceManagement.models.Organizer;
import tsi.ensg.eu.attendanceManagement.services.EventService;
import tsi.ensg.eu.attendanceManagement.services.OrganiserService;

import java.util.ArrayList;
import java.util.List;

@Controller
public class OrganiserController {
    @Autowired
    OrganiserService organiserService;
    @Autowired
    EventService eventService;

    @GetMapping("/joinEventAsOrganiser")
    public String addOrganiser(Model model) {
        /* Creation of an organiser through a form */
        Organizer organiser = new Organizer();
        model.addAttribute("organiser", organiser);
        List<Event> events = eventService.findAll();
        model.addAttribute("listEvents", events);
        return "joinEventAsOrganiser";
    }

    @GetMapping("/joinEventAsOrganiserAdmin")
    public String addOrganiserAdmin(Model model) {
        /* Creation of an organiser through a form */
        Organizer organiser = new Organizer();
        model.addAttribute("organiser", organiser);
        List<Event> events = eventService.findAll();
        model.addAttribute("listEvents", events);
        return "joinEventAsOrganiser";
    }

    @PostMapping("/postJoinEventOrganiser")
    public String submitFormOrganiser(@ModelAttribute("organiser") Organizer organiser) {
        /* Submits info on the created organiser */
        organiserService.save(organiser);
        System.out.println(organiser);
        return "success_join_event_as_organiser";
    }

    @GetMapping("/organisers")
    public String getOrganiser(Model model) {
        /* Display lists of organisers */
        model.addAttribute("organisers", organiserService.findAll());
        return "organisers";
    }

    @GetMapping("/organiser/{id}")
    public String updateFormOrganiser(@PathVariable("id") int id, Model model) {
        /* Display information on a particular organiser */
        Organizer organizer = organiserService.findById((long) (id)).get();
        model.addAttribute("organiser", organizer);

        List<Event> events = eventService.findAll();
        model.addAttribute("listEvents", events);

        return "joinEventAsOrganiser";
    }

    @RequestMapping(value = "/deleteOrganiser/{id}")
    private String deleteOrganizer(@PathVariable(name = "id") int id) {
        /* Deletes organiser */
        Organizer organizer = organiserService.findById((long) (id)).get();

        List<Event> list = new ArrayList<Event>(organizer.getEvents());

        for (Event event : list) {
            organizer.removeEvent(event.getId());
        }
        organiserService.delete(organizer);
        return "redirect:/events";
    }

}
