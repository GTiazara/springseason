package tsi.ensg.eu.attendanceManagement.models;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity(name = "organizer")
public class Organizer extends Participant {
    @Column(name = "phoneNumber", nullable = true)
    private String phoneNumber;
    @Column(name = "title", nullable = true)
    private String title;

    public Organizer() {
    }

    public Organizer(String firstName, String lastName, String emailAddress, String dateBirth, String organisation,
            String observation, Boolean is_Organizer, String phoneNumber, String title) {
        super(firstName, lastName, emailAddress, dateBirth, organisation, observation, is_Organizer);
        this.phoneNumber = phoneNumber;
        this.title = title;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
