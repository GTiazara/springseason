package tsi.ensg.eu.attendanceManagement.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tsi.ensg.eu.attendanceManagement.models.Event;
import tsi.ensg.eu.attendanceManagement.repositories.EventRepository;

import java.util.List;
import java.util.Optional;

@Service
public class EventService {

    @Autowired
    private EventRepository repository;

    public List<Event> findAll() {
        List<Event> events = (List<Event>) repository.findAll();
        return events;
    }

    public void save(Event event) { // pas testé avec persist !
        repository.save(event);
    }

    public Optional<Event> findById(Long id) {
        return repository.findById(id);
    }

    public void delete(Event event) {
        repository.delete(event);
    }
}
