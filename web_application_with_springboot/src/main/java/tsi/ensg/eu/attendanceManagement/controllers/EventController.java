package tsi.ensg.eu.attendanceManagement.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import tsi.ensg.eu.attendanceManagement.models.Event;
import tsi.ensg.eu.attendanceManagement.models.Participant;
import tsi.ensg.eu.attendanceManagement.services.EventService;

import java.util.ArrayList;
import java.util.List;

@Controller
public class EventController {
    @Autowired
    EventService eventService;

    @GetMapping("/events")
    public String getEvent(Model model) {
        /* Display lists of events */
        model.addAttribute("events", eventService.findAll());
        return "event"; // Envoi vers la vue
    }

    @GetMapping("/event/{id}")
    public String getMyArticle(@PathVariable("id") int id, Model model) {
        /* Display information on a particular event */
        model.addAttribute("event", eventService.findById((long) id).get());
        return "event_description";
    }

    @GetMapping("/addEvent")
    public String addEvent(Model model) {
        /* Creation of an event through a form */
        Event participant = new Event();
        model.addAttribute("event", participant);
        return "addEvent";
    }

    @PostMapping("/addEvent")
    public String submitEvent(@ModelAttribute("event") Event event) {
        /* Submits info on the created event */
        eventService.save(event);
        System.out.println(event);
        return "success_add_event";
    }

    @RequestMapping(value = "/deleteEvent/{id}")
    private String deleteEvent(@PathVariable(name = "id") int id) {
        /* Deletes event */
        System.out.println("Student_Id : " + id);
        Event event = eventService.findById((long) (id)).get();
        System.out.println(event.getParticipants().size());

        List<Participant> list = new ArrayList<Participant>(event.getParticipants());

        for (Participant participant : list) {
            event.removeParticipant(participant.getId());
        }
        eventService.delete(event);
        return "redirect:/events";
    }

    @GetMapping("/updateEvent/{id}")
    public String updateForm(@PathVariable("id") int id, Model model) {
        /* Updates event's info by goint back to the form */
        Event event = eventService.findById((long) (id)).get();
        model.addAttribute("event", event);
        return "addEvent";
    }

}
